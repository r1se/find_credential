package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"regexp"
	"time"
)

type config struct {
	Front_port       string `json:"front_port"`
	HttpReadTimeout  int    `json:"httpReadTimeout"`
	HttpWriteTimeout int    `json:"httpWriteTimeout"`

	BleveService struct {
		DBName string `json:"DBName"`
	} `json:"BleveService"`

	PmiService struct {
		Sheduler           int    `json:"sheduler"`
		Timezone_scheduler string `json:"timezone_scheduler"`
		Time_scheduler     string `json:"time_scheduler"`
	} `json:"PmiService"`
}

// LoadConfig load the config.json file and return its data
func loadConfig() *config {
	appConfig := &config{}

	configFile, err := os.Open("config.json")
	defer configFile.Close()
	if err != nil {
		log.Fatalln(err)
	}

	err = json.NewDecoder(configFile).Decode(&appConfig)
	if err != nil {
		log.Fatalln(err)
	}

	return appConfig
}

var (
	tplHome, tplResults *template.Template
	timereg             *regexp.Regexp
	Config              *config
	mngBot              *BotManager
)

const (
	StatusRunned = iota
	StatusStopped
)

func init() {
	timereg = regexp.MustCompile("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")
	Config = loadConfig()
	mngBot = NewBots(Config)
}

func main() {

	err := mngBot.Start()
	if err != nil {
		log.Fatalln(err)
	}
	// get the port to listen on
	port := os.Getenv("PORT")
	if port == "" {
		port = Config.Front_port
	}

	//generate html template
	tplHome = template.Must(template.New(".").Parse(tplStrHome))
	tplResults = template.Must(template.New(".").Parse(tplStrResults))

	//Wrap panic and start http server
	fmt.Printf("Server listening on port %s", port)
	http.Handle("/", RecoverWrap(http.HandlerFunc(commHandler)))
	srv := &http.Server{
		Addr:         ":" + port,
		ReadTimeout:  time.Duration(Config.HttpReadTimeout) * time.Second,
		WriteTimeout: time.Duration(Config.HttpWriteTimeout) * time.Second,
	}
	log.Fatal(srv.ListenAndServe())

}
