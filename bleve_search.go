package main

import (
	"encoding/json"
	"fmt"
	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/document"
	"github.com/blevesearch/bleve/index/store/goleveldb"
	"github.com/blevesearch/bleve/mapping"
	"sync/atomic"
	"time"
)

type answer struct {
	Name       string
	City       string
	Country    string
	Credential string
	Earned     string
	Status     string
}

type BleveService struct {
	bots       *BotManager
	index      bleve.Index
	batchCount int
	batch      *bleve.Batch
	status     uint32
}

func (bs *BleveService) Init(botmng *BotManager) error {
	bs.bots = botmng
	indexName := botmng.config.BleveService.DBName
	index, err := bleve.Open(indexName)
	if err == bleve.ErrorIndexPathDoesNotExist {
		bleve_mapping := bs.buildMapping()
		kvStore := goleveldb.Name
		kvConfig := map[string]interface{}{
			"create_if_missing": true,
		}
		index, err = bleve.NewUsing(indexName, bleve_mapping, "upside_down", kvStore, kvConfig)
	}
	if err != nil {
		return err
	}
	bs.index = index
	bs.batch = index.NewBatch()
	atomic.StoreUint32(&bs.status, StatusRunned)
	return nil
}

func (bs *BleveService) Run() error {
	return nil
}

func (bs *BleveService) Name() string {
	return "bleve_service"
}

func (bs *BleveService) Stop() error {
	atomic.StoreUint32(&bs.status, StatusStopped)
	return nil
}

func (ps *BleveService) Status() uint32 {
	return atomic.LoadUint32(&ps.status)
}

func (bs *BleveService) buildMapping() *mapping.IndexMappingImpl {
	FieldMapping := bleve.NewTextFieldMapping()

	eventMapping := bleve.NewDocumentMapping()
	eventMapping.AddFieldMappingsAt("message", FieldMapping)

	bleve_mapping := bleve.NewIndexMapping()
	bleve_mapping.DefaultMapping = eventMapping
	return bleve_mapping
}

func (bs *BleveService) IndexMessage(data answer) error {
	bs.batch.Index(data.Name, data)
	if bs.batch.Size() > 100 {
		err := bs.index.Batch(bs.batch)
		if err != nil {
			return err
		}
		bs.batchCount += bs.batch.Size()
		bs.batch = bs.index.NewBatch()
	}
	if bs.batch.Size() > 0 {
		bs.index.Batch(bs.batch)
		bs.batchCount += bs.batch.Size()
	}
	return nil
}

func (bs *BleveService) Search(query string, size int) (*bleve.SearchResult, error) {
	stringQuery := fmt.Sprintf("/.*%s.*/", query)
	mq := bleve.NewMatchPhraseQuery(query)
	rq := bleve.NewRegexpQuery(query)
	qsq := bleve.NewQueryStringQuery(stringQuery)
	q := bleve.NewDisjunctionQuery(mq, rq, qsq)
	search := bleve.NewSearchRequest(q)
	search.Size = size
	search.Fields = []string{"name", "city", "country", "credential", "earned", "status"}
	return bs.index.Search(search)
}

func (bs *BleveService) getBleveDocsFromSearchResults(results *bleve.SearchResult) [][]byte {
	docs := make([][]byte, 0)
	for _, val := range results.Hits {
		id := val.ID
		doc, _ := bs.index.Document(id)

		rv := struct {
			ID     string                 `json:"id"`
			Fields map[string]interface{} `json:"fields"`
		}{
			ID:     id,
			Fields: map[string]interface{}{},
		}
		for _, field := range doc.Fields {
			var newval interface{}
			switch field := field.(type) {
			case *document.TextField:
				newval = string(field.Value())
			case *document.NumericField:
				n, err := field.Number()
				if err == nil {
					newval = n
				}
			case *document.DateTimeField:
				d, err := field.DateTime()
				if err == nil {
					newval = d.Format(time.RFC3339Nano)
				}
			}
			existing, existed := rv.Fields[field.Name()]
			if existed {
				switch existing := existing.(type) {
				case []interface{}:
					rv.Fields[field.Name()] = append(existing, newval)
				case interface{}:
					arr := make([]interface{}, 2)
					arr[0] = existing
					arr[1] = newval
					rv.Fields[field.Name()] = arr
				}
			} else {
				rv.Fields[field.Name()] = newval
			}
		}
		j2, _ := json.MarshalIndent(rv.Fields, "", "    ")
		docs = append(docs, j2)
	}

	return docs
}
