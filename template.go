package main

const tplStrHome = `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Allura" rel="stylesheet">
  </head>
  <body>
    <div class="container">
	<div class="row" style="text-align: center">
		<h1 style="padding-top: 1.5em; font-family: Verdana; font-size: 25px">Enter your Query:</h1>
	</div>
	<div class="row" style="padding-top: 1.5em"><div class="col-sm-5 col-sm-offset-4">
		<form action="/" method="GET"><input class="form-control" autofocus name="q" maxlength=100 type="text" placeholder="Name, City, Country, Credential, Earned or Status"></form>
	</div></div>	
	<div class="row" style="padding-top: 2.5em">
	<div class="col-sm-4 col-sm-offset-4">
		<form action="/" method="POST"><input type="submit" class="btn btn-success" name="update" value="Update Button"></form>		
	</div></div>
    </div>
  </body>
</html>
`

const tplStrResults = `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Scenes of Shakespeare</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Allura" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluid">
	<div class="row" style="text-align: center; background-color: #f1f1f1; padding: 1em 0">
		<div class="col-sm-1" style="font-family: Allura, cursive; font-size: 30px">
		<a href="/" style="text-decoration: none; color: #000">SofS</a>
		</div>
		<div class="col-sm-4">
			<form action="/" method="GET"><input class="form-control" autofocus name="q" maxlength=100 type="text" value="{{.Query}}"></form>
		</div>
	</div>
	{{range .Results}}
	<div class="row" style="padding: 1.5em 0; border-bottom: 1px solid #eee"><div class="col-sm-11 col-sm-offset-1">
		<div style="font-size: 1.2em">
				{{.Name}} - City: {{.City}}, {{.Country}} - {{.Credential}}- {{.Earned}} - {{.Status}}		
		</div>
	</div></div>
	{{end}}
	<div class="row" style="padding: 1.5em; background-color: #f1f1f1">
		Text and database from <a href="https://certification.pmi.org/">https://certification.pmi.org/</a>
    </div>
    </div>
  </body>
</html>
`
