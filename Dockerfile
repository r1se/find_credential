FROM golang:latest
RUN mkdir /find_credential
COPY . /go/src/find_credential
WORKDIR /go/src/find_credential

RUN go get github.com/blevesearch/bleve
RUN go get github.com/blevesearch/bleve/document
RUN go get github.com/blevesearch/bleve/index/store/goleveldb
RUN go get github.com/jasonlvhit/gocron
RUN go get github.com/PuerkitoBio/goquery
RUN go get googlemaps.github.io/maps

RUN go build
CMD ["find_credential"]
EXPOSE 8080