package main

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
)

//Wrap some panics, because http must run forever
func RecoverWrap(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error
		defer func() {
			r := recover()
			if r != nil {
				switch t := r.(type) {
				case string:
					err = errors.New(t)
				case error:
					err = t
				default:
					err = errors.New("Unknown error")
				}
				log.Println(err.Error())
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		}()
		h.ServeHTTP(w, r)
	})
}

func commHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {
		if r.FormValue("update") == "Update Button" {
			if mngBot.PmiService().status != StatusRunned {
				go mngBot.PmiService().Run()
			}
		}
		tplHome.Execute(w, nil)
		return
	}

	q := r.FormValue("q")
	if q == "" {
		tplHome.Execute(w, nil)
		return
	}

	if len(q) > 100 {
		q = q[:100]
	}

	res, err := mngBot.BleveService().Search(q, 100)
	if err != nil {
		http.Error(w, "not found", 404)
		return
	}
	answerbyte := mngBot.BleveService().getBleveDocsFromSearchResults(res)

	results := []answer{}
	for _, elem := range answerbyte {
		tmp := answer{}
		json.Unmarshal(elem, &tmp)
		results = append(results, tmp)
	}

	tplResults.Execute(w, map[string]interface{}{
		"Results": results,
		"Query":   q,
	})
}
