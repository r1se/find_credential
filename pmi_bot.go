package main

import (
	"context"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/jasonlvhit/gocron"
	"googlemaps.github.io/maps"
	"log"
	"net/http"
	"net/url"
	"reflect"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

type PmiService struct {
	bots      *BotManager
	gplace    map[string]string
	mxplace   sync.Mutex
	waitGroup sync.WaitGroup
	status    uint32
}

func (ps *PmiService) Init(botmng *BotManager) error {
	ps.bots = botmng
	ps.gplace = make(map[string]string)
	atomic.StoreUint32(&ps.status, StatusStopped)
	//Start scheduler for update task
	if botmng.config.PmiService.Sheduler == 1 {

		//Load time location from config
		go func() {
			//gocron.Clear()
			newloc, err := time.LoadLocation(botmng.config.PmiService.Timezone_scheduler)
			if err != nil {
				log.Println("Wrong time zone. Must be IANA Time Zone: " + err.Error())
			} else {
				//Set timezone location
				gocron.ChangeLoc(newloc)
			}
			if !timereg.MatchString(botmng.config.PmiService.Time_scheduler) {
				log.Println("Wrong time format. Schedule not started.")
			} else {
				//Start cron work
				//gocron.Every(1).Day().At(botmng.config.PmiService.Time_scheduler).Do(ps.Run())
				//<-gocron.Start()
			}
		}()
	}

	return nil
}

func parse(ps *PmiService, letter string, queue chan []answer) {
	//stop wait
	defer ps.waitGroup.Done()

	//Get aspx types
	resp, err := http.Get("https://certification.pmi.org/registry.aspx")
	if err != nil {
		fmt.Println("errorination happened getting the response", err)
		return
	}
	defer resp.Body.Close()
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		fmt.Println("errorination happened getting the response", err)
		return
	}
	var viewstate, viewstategeneartor string

	doc.Find("input").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		if href, ok := s.Attr("name"); ok {
			if href == "__VIEWSTATE" {
				if value, ok := s.Attr("value"); ok {
					viewstate = value
				}
			}
			if href == "__VIEWSTATEGENERATOR" {
				if value, ok := s.Attr("value"); ok {
					viewstategeneartor = value
				}
			}
		}
	})

	//Get Data
	country := "RUS"
	resp, err = http.PostForm("https://certification.pmi.org/registry.aspx",
		url.Values{"__EVENTTARGET": {""}, "__EVENTARGUMENT": {""}, "__VIEWSTATE": {viewstate}, "__VIEWSTATEGENERATOR": {viewstategeneartor},
			"dph_RegistryContent$tbSearch": {letter}, "dph_RegistryContent$firstNameTextBox:": {""},
			"dph_RegistryContent$wcountry": {country}, "dph_RegistryContent$credentialDDL": {"0"}, "dph_RegistryContent$Button1": {"Search"}})
	if err != nil {
		fmt.Println("errorination happened getting the response", err)
		return
	}

	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		fmt.Println("errorination happened getting the response", err)
		return
	}

	//Initialize slice
	var slcstruct []answer
	var tmpslice []string

	//Get size of element in struct answer
	sizestruct := reflect.ValueOf(&answer{}).Elem().NumField()

	//Get each td from html
	doc.Find("td").Each(func(i int, s *goquery.Selection) {
		// For each item found, check empty space
		tmp := strings.TrimSpace(s.Text())
		if tmp == "" {
			pos := i - len(slcstruct)*sizestruct
			//get field
			field := reflect.ValueOf(&slcstruct[len(slcstruct)-1]).Elem().Field(pos)
			tmp = field.Interface().(string)
			/*// Ignore fields that don't have the same type as a string
			if field.Type() != reflect.TypeOf("") {
				continue
			}*/

		}
		tmpslice = append(tmpslice, tmp)

		//add answer to slice
		if len(tmpslice) == sizestruct {
			data := answer{}
			for index, value := range tmpslice {
				reflect.ValueOf(&data).Elem().Field(index).SetString(value)
			}
			data.City, err = ps.ValidatePlace(data.City)
			if err != nil {
				fmt.Println("error validate address", err)
			}
			fmt.Println(data.City)
			slcstruct = append(slcstruct, data)
			tmpslice = nil
		}

	})
	queue <- slcstruct
}

func (ps *PmiService) Run() error {
	atomic.StoreUint32(&ps.status, StatusRunned)

	letters := []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}
	queue := make(chan []answer, 26)
	for _, letter := range letters {
		ps.waitGroup.Add(1)
		go parse(ps, letter, queue)
	}

	ps.waitGroup.Wait()

	for elem := range queue {
		for _, val := range elem {
			ps.bots.BleveService().IndexMessage(val)
		}
	}
	atomic.StoreUint32(&ps.status, StatusStopped)
	return nil
}

func (ps *PmiService) Name() string {
	return "pmi_service"
}

func (ps *PmiService) Stop() error {
	atomic.StoreUint32(&ps.status, StatusStopped)
	return nil
}

func (ps *PmiService) Status() uint32 {
	return atomic.LoadUint32(&ps.status)
}

func (ps *PmiService) ValidatePlace(place string) (string, error) {

	//Get google client object
	c, err := maps.NewClient(maps.WithAPIKey("AIzaSyBl9ezTDIhtjOkeKl6n5Df7po4sV9zopFI"))
	if err != nil {
		return place, err
	}

	//Make place autocomplete response
	r := maps.PlaceAutocompleteRequest{
		Input:    place,
		Types:    maps.AutocompletePlaceTypeCities,
		Language: "en",
	}
	response, err := c.PlaceAutocomplete(context.Background(), &r)
	if err != nil {
		return place, err
	}

	//compare with exist ID on map
	ps.mxplace.Lock()
	defer ps.mxplace.Unlock()
	if _, ok := ps.gplace[response.Predictions[0].PlaceID]; !ok {
		ps.gplace[response.Predictions[0].PlaceID] = response.Predictions[0].StructuredFormatting.MainText
		place = response.Predictions[0].StructuredFormatting.MainText
	} else {
		place = ps.gplace[response.Predictions[0].PlaceID]
	}
	return place, nil
}
