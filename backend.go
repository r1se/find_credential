package main

import (
	"fmt"
	"log"
)

type BotManager struct {
	config   *config
	services map[string]Service
}

type Service interface {
	Init(*BotManager) error
	Run() error
	Name() string
	Stop() error
	Status() uint32
}

// NewFb2Telegram creates and returns new Fb2TelegramInstance
func NewBots(config *config) *BotManager {
	bman := new(BotManager)
	bman.config = config
	bman.services = make(map[string]Service)

	//add config loads bot
	bman.AddService(&PmiService{})
	bman.AddService(&BleveService{})
	return bman
}

// Start starts all services in separate goroutine
func (pb *BotManager) Start() error {
	log.Println("Starting bot service")
	for _, service := range pb.services {
		log.Println("Initializing: %v\n", service.Name())
		if err := service.Init(pb); err != nil {
			return fmt.Errorf("initialization of %q finished with error: %v", service.Name(), err)
		}
	}

	return nil
}

// AddService adds service into Fb2Telegram.services map
func (pb *BotManager) AddService(srv Service) {
	pb.services[srv.Name()] = srv
}

// Stop stops all services running
func (pb *BotManager) Stop() {
	log.Println("Worker is stopping...")
	for _, service := range pb.services {
		service.Stop()
	}
}

func (pb *BotManager) PmiService() *PmiService {
	service, ok := pb.services["pmi_service"]
	if !ok {
		log.Println("Error getting pmi_service")
	}
	return service.(*PmiService)
}

func (pb *BotManager) BleveService() *BleveService {
	service, ok := pb.services["bleve_service"]
	if !ok {
		log.Println("bleve_service service not found")
	}
	return service.(*BleveService)
}
